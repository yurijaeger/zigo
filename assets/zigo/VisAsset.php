<?php

namespace app\assets\zigo;

use yii\web\AssetBundle;

class VisAsset extends AssetBundle
{

    public $sourcePath = '@npm/vis';

    public $css = [
        'dist/vis.css'
    ];
    public $js = [
        'dist/vis.js',
        '../../js/zigo/mapNodes.js',
        '../../js/zigo/mapEdges.js',
        '../../js/zigo/mapUtils.js',
        '../../js/zigo/map.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}