<?php

use app\assets\zigo\VisAsset;

VisAsset::register($this);

$this->title = 'Map';


// TODO: Transpor o CSS daqui para um Asset externo

?>
<style type="text/css">
    #zigomap {
        /*width: 100%;*/
        height: 800px;
        border: 1px solid lightgray;
        align: center;
    }

    #edge-popUp {
        display: none;
        position: absolute;
        top: 350px;
        z-index: 299;
    }
    #edge-popUpheader {
        z-index: 300;
        cursor: move;
    }
</style>


<div id="edge-popUp" class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-primary" id="panel-popup">
                <div class="panel-heading">
                    <h3 id="edge-popUpheader" class="panel-title"><span id="edge-operation">edge</span></h3>
                </div>

                <div class="panel-body">

                    <form class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <label class="col-sm-12" style="font-weight: normal !important;">Selecione o que deseja monitorar para este host:</label>
                        </div>
                        <div class="form-group">
                            <label for="selectSt" class="col-sm-3 control-label">Tráfego:</label>
                            <div class="col-sm-9">
                                <select name="selectSt" class="form-control" id="selectSt"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="button" class="btn btn-success" value="Salvar" id="edge-saveButton"/>
                                <input type="button" class="btn btn-danger" value="Cancelar" id="edge-cancelButton"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-7">

        </div>
    </div>
</div>
<div id="zigomap"></div>
