<?php

namespace app\controllers;

use app\models\Hosts;
use app\models\MapEdges;
use app\models\MapNodes;
use app\models\Servicesin;
use app\models\Servicesout;
use app\models\Traffic;
use app\models\Trafficout;
use yii\helpers\Json;
use yii\web\Controller;
use yii\db\Query;

class MapController extends Controller
{

    public function actionIndex()
    {
        $hosts = Hosts::find()
            ->select('id,nome')
            ->orderBy('id')
            ->all();

        return $this->render('index', [
            'hosts' => $hosts,
        ]);
    }

    public function  actionGetHosts() {
        $hosts = (new Query())
            ->select('hosts.id, hosts.nome, map_nodes.x, map_nodes.y')
            ->from("hosts")
            ->join('LEFT JOIN','map_nodes',"hosts.id = map_nodes.host_id")
            ->orderBy('id')
            ->all();

        echo Json::encode($hosts);
        exit;
    }

    public function  actionGetEdges() {
        $edges = MapEdges::find()
            ->all();

        echo Json::encode($edges);
        exit;
    }

    public function actionGetNodes() {

        $nodes = (new Query())
            ->select('hosts.id, hosts.nome, servicesin.type, servicesin.status, servicesin.active, map_nodes.x, map_nodes.y')
            ->from('hosts')
            ->join('INNER JOIN','servicesin',"hosts.id = servicesin.host_id")
            ->join('LEFT JOIN','map_nodes',"hosts.id = map_nodes.host_id")
            ->orderBy(["hosts.id" => SORT_ASC, "servicesin.type" => SORT_ASC, "servicesin.status" => SORT_ASC])
            ->all();

        echo Json::encode($nodes);
        exit;
    }

    public function actionGetEdgesTraffic() {

        $traffic = (new Query())
            ->select("map_edges.id, traffic.rx, traffic.tx")
            ->from("map_edges")
            ->join('LEFT JOIN','traffic',"map_edges.traffic_id = traffic.id") // TYPE 0 = PING
            ->all();

        echo Json::encode($traffic);

        exit;
    }

    public function actionGetHostTraffic($id) {

        $traffic = Traffic::find()
            ->select("id, name")
            ->where("host_id = " . $id . "")
            ->all();

        echo Json::encode($traffic);

        exit;
    }

    public function actionUpdateHostPosition($hostId,$x,$y) {

        $map = MapNodes::find()
            ->where("host_id = " . $hostId)
            ->one();

        if ($map == null) {
            $mapInsert = new MapNodes();
            $mapInsert->host_id = $hostId;
            $mapInsert->x = $x;
            $mapInsert->y = $y;
            $mapInsert->save();
            echo "Inserido";
        }
        else {

            $map->x = $x;
            $map->y = $y;
            $map->save();
            echo "Updateado";

        }

        exit;

    }

    public function actionCreateEdge($fromId,$toId,$trafficId) {

        $mapInsert = new MapEdges();
        $mapInsert->host_id = $fromId;
        $mapInsert->host_dest_id = $toId;
        $mapInsert->traffic_id = $trafficId;
        if ($mapInsert->save() ) {
            echo Json::encode($mapInsert->id);
        }
        exit;

    }
    public function actionDeleteEdge($edgeId) {

        $mapDelete = MapEdges::find()
            ->where(['id' => $edgeId])->one();

        if($mapDelete->delete()) {
            echo Json::encode($mapDelete->id);
        }

        exit;
    }
    public function actionEditEdge($edgeId, $trafficId) {

        $mapUpdate = MapEdges::find()
            ->where(['id' => $edgeId])->one();;
        $mapUpdate->traffic_id = $trafficId;
        if ($mapUpdate->save() ) {
            echo Json::encode($mapUpdate);
        }
        exit;
    }
}
