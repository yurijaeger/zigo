<?php

return [
    'adminEmail' => 'admin@example.com',
    'ZIGO_MAP_MAX_CHARS_WRAP' => 16,    // Numero de caracteres maximo por linha de cada Node no Mapa
];
