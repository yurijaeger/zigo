<?php

namespace app\models;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hosts".
 *
 * @property integer $id
 * @property string $nome
 * @property string $ip
 * @property string $services
 */
class MapEdges extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_edges';
    }
}
