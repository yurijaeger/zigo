<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hosts".
 *
 * @property integer $id
 * @property string $nome
 * @property string $ip
 * @property string $services
 */
class Hosts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hosts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'ip', 'services'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'ip' => 'Ip',
            'services'=>'Serviços Ativos',
            //    'function_id'=>'Função',
            //     'notify'=>'Notificação',
            //    'not_interval'=>'Intervalo',
        ];
    }
}
