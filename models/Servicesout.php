<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicesout".
 *
 * @property integer $id
 * @property integer $hosts_id
 * @property string $name
 * @property string $returnx
 * @property string $status
 * @property string $day
 * @property string $month
 * @property string $year
 * @property string $hour
 * @property string $minute
 * @property string $datetime
 */
class Servicesout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicesout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hosts_id'], 'integer'],
            [['datetime'], 'safe'],
            [['name', 'returnx'], 'string', 'max' => 100],
            [['status', 'day', 'month', 'year', 'hour', 'minute'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hosts_id' => 'Hosts ID',
            'name' => 'Name',
            'returnx' => 'Returnx',
            'status' => 'Status',
            'day' => 'Day',
            'month' => 'Month',
            'year' => 'Year',
            'hour' => 'Hour',
            'minute' => 'Minute',
            'datetime' => 'Datetime',
        ];
    }
}
