<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trafficout".
 * a
 * @property integer $id
 * @property integer $host_id
 * @property string $name
 * @property string $returnrx
 * @property string $returntx
 * @property string $status
 * @property string $time
 * @property string $date
 */
class Trafficout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trafficout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host_id', 'name', 'returnrx', 'returntx', 'status', 'time', 'date'], 'required'],
            [['host_id'], 'integer'],
            [['time', 'date'], 'safe'],
            [['name', 'returnrx', 'returntx', 'status'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host_id' => 'Host ID',
            'name' => 'Name',
            'returnrx' => 'Returnrx',
            'returntx' => 'Returntx',
            'status' => 'Status',
            'time' => 'Time',
            'date' => 'Date',
        ];
    }
}
