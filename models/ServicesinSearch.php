<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Servicesin;

/**
 * ServicesinSearch represents the model behind the search form about `app\models\Servicesin`.
 */
class ServicesinSearch extends Servicesin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'host_name', 'type', 'version', 'community', 'oid', 'function_id', 'email', 'telegram'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servicesin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'host_name', $this->host_name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'community', $this->community])
            ->andFilterWhere(['like', 'oid', $this->oid])
            ->andFilterWhere(['like', 'function_id', $this->function_id])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telegram', $this->telegram]);

        return $dataProvider;
    }
}
