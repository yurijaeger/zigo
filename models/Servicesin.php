<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicesin".
 *
 * @property integer $id
 * @property string $name
 * @property string $host_name
 * @property string $type
 * @property string $version
 * @property string $community
 * @property string $oid
 * @property string $function_id
 * @property string $email
 * @property string $telegram
 */
class Servicesin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicesin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'host_name', 'type'], 'required'],
            [['name', 'host_name', 'community', 'oid', 'function_id'], 'string', 'max' => 100],
            [['type', 'version', 'email', 'telegram'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host_name' => 'Nome do Host',
            'name' => 'Nome do Serviço',
            'type' => 'Tipo de Conexão',
            'version' => 'Versão do SNMP',
            'community' => 'Comunidade do SNMP',
            'oid' => 'OID',
            'function_id'=>'Função',
            'email' => 'Notificar via e-mail',
            'telegram' => 'Notificar via Telegram',
            'status' => 'Status',
        ];
    }
}
