<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Servicesout;

/**
 * ServicesoutSearch represents the model behind the search form about `app\models\Servicesout`.
 */
class ServicesoutSearch extends Servicesout
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hosts_id'], 'integer'],
            [['name', 'returnx', 'status', 'day', 'month', 'year', 'hour', 'minute', 'datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servicesout::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hosts_id' => $this->hosts_id,
            'datetime' => $this->datetime,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'returnx', $this->returnx])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'day', $this->day])
            ->andFilterWhere(['like', 'month', $this->month])
            ->andFilterWhere(['like', 'year', $this->year])
            ->andFilterWhere(['like', 'hour', $this->hour])
            ->andFilterWhere(['like', 'minute', $this->minute]);

        return $dataProvider;
    }
}
