<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "traffic".
 *
 * @property integer $id
 * @property string $host_name
 * @property string $name
 * @property string $version
 * @property string $community
 * @property string $tx_oid
 * @property string $rx_oid
 * @property integer $band
 */
class Traffic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'traffic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host_name', 'name', 'version', 'community', 'tx_oid', 'rx_oid'], 'required'],
            //[['band'], 'integer'],
            [['host_name', 'name', 'version', 'community'], 'string', 'max' => 100],
            [['tx_oid', 'rx_oid'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host_name' => 'Nome do host',
            'name' => 'Nome do gráfico',
            'version' => 'Versão do SNMP',
            'community' => 'Comunidade do SNMP',
            'tx_oid' => 'Tx OID',
            'rx_oid' => 'Rx OID',
           // 'band' => 'Multiplicador',
        ];
    }
}
