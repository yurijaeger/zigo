
const   STATUS_INACTIVE = 0,
        STATUS_OFFLINE = 1,
        STATUS_SNMP_WARN = 2,
        STATUS_ONLINE = 4,
        TYPE_PING = 0,
        TYPE_SNMP = 1;

// Faz o update do status (cores) de cada host (node) na tela conforme status de servicos
function updateNodes(allNodes) {

    var color = [];

    for (node of allNodes) {

        // Busca as informacoes do node existente na tela do Vis.JS
        var existingNode = nodes.get(node.id);

        var newStatus = STATUS_INACTIVE;

        if (node.type == TYPE_PING) {
            if (node.active == 0) {
                newStatus = STATUS_INACTIVE;
                color = {
                    background: COLOR_MAIN_INACTIVE,
                    border: COLOR_BORDER_INACTIVE,
                    highlight: {
                        border: "#85dbff",
                        background: COLOR_MAIN_INACTIVE
                    },
                    hover: {
                        background: "#d3dd1d"
                    }
                };
                nodes.update([{id:node.id, status: newStatus, color:color}]);
            }
            else {
                if (node.status == 1) {
                    newStatus = STATUS_OFFLINE;
                    color = {
                        background: COLOR_MAIN_OFFLINE,
                        border: COLOR_BORDER_OFFLINE,
                        highlight: {
                            border: "#85dbff",
                            background: COLOR_MAIN_OFFLINE
                        },
                        hover: {
                            background: "#d3dd1d"
                        }
                    };
                    nodes.update([{id:node.id, status: newStatus, color:color}]);
                }
                else {
                    //if (existingNode.status != STATUS_SNMP_WARN) {
                        newStatus = STATUS_ONLINE;
                        color = {
                            background: COLOR_MAIN_ONLINE,
                            border: COLOR_BORDER_ONLINE,
                            highlight: {
                                border: "#85dbff",
                                background: COLOR_MAIN_ONLINE
                            },
                            hover: {
                                background: "#d3dd1d"
                            }
                        };
                        nodes.update([{id:node.id, status: newStatus, color:color}]);
                    //}
                }
            }
        }
        else {
            if (node.active == TYPE_SNMP) {
                if (node.status == 1) {
                    if ((existingNode.status != STATUS_INACTIVE) && (existingNode.status != STATUS_OFFLINE)) {
                        newStatus = STATUS_SNMP_WARN;
                        color = {
                            background: COLOR_MAIN_SNMP,
                            border: COLOR_BORDER_SNMP,
                            highlight: {
                                border: "#85dbff",
                                background: COLOR_MAIN_SNMP
                            },
                            hover: {
                                background: "#d3dd1d"
                            }
                        };
                        nodes.update([{id:node.id, status: newStatus, color:color}]);
                    }
                }
            }
        }
    }
}

// Atualiza a posicao X,Y do node no banco ao ser arrastado
function updateNodePosition(hostId, x, y) {

    url = 'index.php?r=map/update-host-position'

    $.ajax({ type: "GET",
        url: url,
        async: true,
        data: {
            hostId: hostId,
            x: x,
            y: y
        },
        success : function(text)
        {

        }
    });
}

// Monta o objeto de nodes e os parametros para o Vis.JS
function createNodes() {

    var nodesStr = [];

    hosts = getMapInfo("hosts");

    color = {
        background: COLOR_MAIN_INACTIVE,
        border: COLOR_BORDER_INACTIVE,
        highlight: {
            border: "#85dbff",
            background: COLOR_MAIN_INACTIVE
        },
        hover: {
            background: "#d3dd1d"
        }
    };

    for (host of hosts) {

        if (host.x == null) host.x = 0;
        if (host.y == null) host.y = 0;

        nodesStr.push({id: host.id, label: wrap(host.nome, 10), x: host.x, y: host.y, color: color, margin: 25});

    }
    return nodesStr;
}