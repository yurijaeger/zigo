// Adicionar novo edge no banco e na tela
function addNewEdge(fromId, toId, trafficId) {

    url = 'index.php?r=map/create-edge'

    $.ajax({ type: "GET",
        url: url,
        async: false,
        data: {
            fromId: fromId,
            toId: toId,
            trafficId: trafficId,
        },
        success : function(text)
        {
            edgeId = $.parseJSON(text);
        }
    });

    edges.add({
        id: edgeId,
        from: fromId,
        to: toId
    });
}

// Remover edge selecionado no banco e na tela
function removeEdge(edgeId) {

    url = 'index.php?r=map/delete-edge'

    $.ajax({ type: "GET",
        url: url,
        async: true,
        data: {
            edgeId: edgeId[0]
        },
        success : function(text)
        {
            var info = $.parseJSON(text);
        }
    });

    edges.remove(edgeId);
}

// Update do traffic do edge editado no banco
function editEdge(edgeId, trafficId) {
    url = 'index.php?r=map/edit-edge'

    $.ajax({ type: "GET",
        url: url,
        async: true,
        data: {
            edgeId: edgeId,
            trafficId: trafficId
        },
        success : function(text)
        {

        }
    });
}

// Busca no banco os edges e cria o objeto de criacao de egdes para o Vis.JS
function createEdges() {

    var edgesd = getMapInfo("edges");

    var edgesStr = [];

    for (edge of edgesd) {

        edgesStr.push({
            id: edge.id,
            from: edge.host_id,
            to: edge.host_dest_id,
            trafficId: edge.traffic_id,
        });
    }

    return edgesStr;
}

// Busca as informacoes de trafego de dados para cada host no banco e atualiza os labels dos edges de acordo
function updateTraffic(traffics) {

    for (traffic of traffics) {
        // Se não ouver informacao deixa em branco o edge
        if ((traffic.rx == null) || (traffic.tx == null)) {
            edges.update([{id: traffic.id, label: " "}]);
        }
        // Caso haja preenche com RX e TX
        else {
            edges.update([{
                id: traffic.id,
                label: "RX: " + bytesToSize(traffic.rx) + "\nTX: " + bytesToSize(traffic.tx)
            }]);
        }
    }

}