// Busca no banco as informacoes dos Hosts
// TODO: Remover em futura revisao para utilizar assincrono
function getMapInfo(endpoint) {

    var info = '';

    url = 'index.php?r=map/get-' +  endpoint;

    $.ajax({ type: "GET",
        url: url,
        async: false,
        success : function(text)
        {
            info = $.parseJSON(text);
        }
    });

    return info;
};

// Funcao de quebra automatica de linha
// TODO: Melhorar quebra de linha pra separar primeiro por espacos, depois quebrar o restante
function wrap (text, limit) {
    if (text.length > limit) {

        // Quebra nos espacos
        //var edge = text.slice(0, limit).lastIndexOf(' ');

        var edge = LINE_MAX_CHARS; // Numero maximo de caracteres por linha

        if (edge > 0) {
            var line = text.slice(0, edge);
            var remainder = text.slice(edge + 1);
            return line + '\n' + wrap(remainder, limit);
        }
    }
    return text;
}

// Formata um numero em bytes pra a unidade correta
function bytesToSize(bytes) {

    var sizes = ['B/s', 'kB/s', 'MB/s', 'GB/s', 'TB/s'];

    if (bytes == 0) return '0 B/s';

    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];

};


// Faz a requisicao assincrona de dados no banco atraves do endpoint indicado
function getStatusAsync(endpoint, callback) {

    url = 'index.php?r=map/' +  endpoint;

    $.ajax({ type: "GET",
        url: url,
        cache: false,
        async: true,
        success : function(text)
        {
            callback($.parseJSON(text));
        }
    });
}

// Funcoes para fazer a janela de edicao ser arrastavel
dragElement(document.getElementById("edge-popUp"));

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {

        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {

        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();

        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;

        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();

        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;

        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {

        document.onmouseup = null;
        document.onmousemove = null;
    }
}