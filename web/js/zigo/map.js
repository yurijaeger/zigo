const   LINE_MAX_CHARS = 16,   // Numero maximo de caracteres por linha num node (para Wrap)
        COLOR_MAIN_OFFLINE = "#FF5252",
        COLOR_MAIN_INACTIVE = "#f2f2f2",
        COLOR_BORDER_OFFLINE = "#b93c3c",
        COLOR_BORDER_INACTIVE = "#a8a8a8",
        COLOR_MAIN_ONLINE = "#7fec4b",
        COLOR_BORDER_ONLINE = "#498b2d",
        COLOR_MAIN_SNMP = "#FFA000",
        COLOR_BORDER_SNMP = "#855300",
        COLOR_EDGE = "#c2ecff",
        COLOR_EDGE_SELECTED = "#80d6fa",
        UPDATE_INTERVAL = 5000; // Update dos dados de 5 em 5 segundos

// Variavel auxiliar para indicar se o usuario esta ou nao arrastando a tela
var dragging = 0;

// Funcao de update da tela
function updateScreen() {

    getStatusAsync("get-nodes", updateNodes);
    getStatusAsync("get-edges-traffic", updateTraffic);

}

// Update da tela a cada UPDATE_INTERVAL milisegundos
// TODO: Melhorar desempenho, verificar travamentos
window.setInterval(function(){

    if (dragging != 1) {
        updateScreen();
    }
}, UPDATE_INTERVAL);


// Criacao array de nodes (hosts)
var nodes = new vis.DataSet(createNodes());

// Criacao dos edges (conexoes)
var edges = new vis.DataSet(createEdges());

// ID do Elemento (DIV) utilizado no index.php para criar o mapa
var container = document.getElementById('zigomap');

// Dados para o Vis.js
var data = {
    nodes: nodes,
    edges: edges,
};

// Objeto de configuracao do VIS.JS
var options = {

    locales: {
        zigo: {
            edit: 'Editar',
            del: 'Remover Conexão',
            back: 'Voltar',
            addNode: 'Adicionar Host',
            addEdge: 'Adicionar Conexão',
            editNode: 'Editar Host',
            editEdge: 'Editar Conexão',
            addDescription: 'Clique em um espaço vazio para adicionar um novo host',
            edgeDescription: 'Clique em um Host e arraste a conexão até o host destino',
            editEdgeDescription: 'Clique nas pontas de controle para modificar a conexão',
            createEdgeError: 'Erro 1',
            deleteClusterError: 'Erro 2',
            editClusterError: 'Erro 3'
        }
    },

    locale: "zigo",

    nodes: {
        shape: "box",
        shapeProperties: {
            borderRadius: 34
        },
        borderWidth: 3,
        borderWidthSelected: 6,
        scaling: {
            min: 10,
            max: 100,
        },
        font: {
            face: "verdana",
        },
        labelHighlightBold: false,
    },

    edges: {
        value: 5,
        color: {
            color: COLOR_EDGE,
            highlight: COLOR_EDGE_SELECTED
        },
        font: {
            strokeWidth: 5,
            size: 70,
            strokeColor : '#ffffff'
        },
        scaling: {
            min: 1,
            max: 50,
            label: {
                min: 1,
                max: 50,
            },
        },
        smooth: false
    },

    manipulation: {
        addNode: false,

        addEdge: function (data, callback) {

            if (data.from == data.to) {
                alert("Não é possível conectar ao mesmo Host");
                callback(null);
                return;
            }

            document.getElementById('edge-operation').innerHTML = "Adicionar Conexão";
            editEdgePopup(data, callback, 0);

        },
        editEdge: {
            editWithoutDrag: function(data, callback) {

                document.getElementById('edge-operation').innerHTML = "Editar Conexão";
                editEdgePopup(data, callback, 1);

            }
        },

        deleteEdge: function (data, callback) {

            deleteEdge(data, callback);

        },
        deleteNode: false
    },

    physics: {
        enabled: false
    }
};


// Inicializa o mapa
var network = new vis.Network(container, data, options);

// Faz o update logo no inicio da execucao
updateScreen();

// Seta a flag de arrasto de tela ao iniciar o arraste
network.on("dragStart", function (params) {
    params.event = "[original event]";
    dragging = 1;

});

// Faz o update da posicao de um node ao terminar de ser arrastado, e update da tela
network.on("dragEnd", function (params) {
    params.event = "[original event]";

    dragging = 0;
    
    // Pega o node dos parametros do DOM
    var nodeId = this.getNodeAt(params.pointer.DOM)

    // Checa se existe id para algum node para identificar se o drag é de node ou canvas
    if (nodeId != null) {

        // Busca a posicao XY do node correspondente dentro da network
        var posXY = network.getPositions([nodeId]);

        updateNodePosition(nodeId, posXY[nodeId].x, posXY[nodeId].y);
        console.log("X: ", posXY[nodeId].x, "Y: ", posXY[nodeId].y);

    }
    updateScreen();
});


// Exibe o popup de edicao/adicao de edges. action 0 = adicao, action 1 = edicao.
function editEdgePopup(data, callback, action) {

    if (typeof data.to === 'object') {
        data.to = data.to.id;
    }

    if (typeof data.from === 'object') {
        data.from = data.from.id;
    }

    // console.log("Data: ", data);
    // console.log("Callback: ", callback);

    document.getElementById('edge-saveButton').onclick = saveEdgeData.bind(this, data, action);
    document.getElementById('edge-cancelButton').onclick = cancelEdgeEdit.bind(this, callback);
    document.getElementById('edge-popUp').style.display = 'block'

    $.ajax({
        url:'index.php?r=map/get-host-traffic',
        data: {
            id: data.from,
        },
        type:'GET',
        dataType: 'json',
        success: function( json ) {
            console.log("DADOS: ", json)
            $.each(json, function(i, value) {
                $('<option></option>', {text:value.name}).attr('value', value.id).appendTo('#selectSt');
            });
            $('<option></option>', {text:"Nenhum"}).attr('value', '0').appendTo('#selectSt');
        }
    });

    callback(null);

}

// Salva os dados de edge no banco
function saveEdgeData(data, action) {

    var trafficId = document.getElementById('selectSt').value;

    if (action == 0) {

        // console.log("CREATE EDGE From: ", data.from);
        // console.log("TO: ", data.to);
        // console.log("ServiceiD: ", trafficId);

        addNewEdge(data.from, data.to, trafficId);
    }
    else {
        editEdge(data.id, trafficId);
        // console.log("UPDATE EDGE: ", data.id);
        // console.log("ServiceiD: ", trafficId);
    }
    clearEdgePopUp();
    updateScreen();

}

function deleteEdge(data, callback) {
    edges.remove(data.edges);
    removeEdge(data.edges);
}

function cancelEdgeEdit(callback) {
    clearEdgePopUp();
    callback(null);
}

// Esconde o popup de edicao/adicao de edge
function clearEdgePopUp() {
    document.getElementById('edge-saveButton').onclick = null;
    document.getElementById('edge-cancelButton').onclick = null;
    document.getElementById('edge-popUp').style.display = 'none';
    $('#selectSt')
        .find('option')
        .remove()
        .end()
    ;
}